//
$(function () {
    $("#allForms").submit(function () {
    	var getCampos = $("input[temQueSerPreenchido]");
    	
        var campos = getCampos.filter(function() {
            return !this.value;
        }).get();

        if (campos.length) {
            alert("ATENÇÃO!! Todos os campos devem ser preenchidos.");
            return false;
        }
    });
});