# Ivia

Teste Técnico da Ivia para desenvolvedor JAVA


Criar o Schema teste1 no banco e executar os seguintes comandos SQL.

CREATE TABLE `usuario` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `senha` varchar(30) DEFAULT NULL,
  `ddd` int(3) DEFAULT NULL,
  `numero` varchar(12) DEFAULT NULL,
  `tipo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

LOCK TABLES `usuario` WRITE;

INSERT INTO `usuario` VALUES (1,'hugh jackman','jugh@wolverine.com.net','senha123',11,'99999992','Número da mae'),(2,'hugo','hugo@hugo.net','senha123',81,'11111118','fixo'),(3,'carlos','carlos@carlinhos.com.br','2344221',41,'9224252','numero do irmao'),(6,'admin','admin@admin','admin',81,'7777777','admin');


PARA LOGAR APÓS EXECUTAR OS COMANDOS, USAR login: "hugo@hugo.net" e a senha: "senha123" 