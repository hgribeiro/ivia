<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<title>Cadastrar-se</title>
<script type="text/javascript">
	$(document).ready(function() {
		$("#idDddCadastrar").keyup(function() {
		    $("#idDddCadastrar").val(this.value.match(/[0-9]*/));
		});
	});
</script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <a class="navbar-brand text-light" href="https://www.ivia.com.br/" target="_blank"><img scr="https://pbs.twimg.com/profile_images/783097273848827904/FlQ4jxAe_400x400.jpg"><i>IVIA - CRUD</i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Alterna navega��o">
    <span class="navbar-toggler-icon"></span>
  </button>
</nav>
<div class="d-flex justify-content-center">
	<form id="allForms" action="usuario" method="post" class="mt-5 p-4 border" style="width: 35rem;">
	<input type="hidden" name="option" value="cadastrar"></input>
	<h4 class="mb-4">Preencha os campo abaixo:</h4>
	  <div class="form-group">
	    <label for="inputAddress">Nome</label>
	    <input type="text" class="form-control" name="nome" id="idNomeCadastrar" temQueSerPreenchido placeholder="Fulano da Silva">
	  </div>
	  <div class="form-row">
	    <div class="form-group col-md-6">
	      <label for="inputEmail4">Email</label>
	      <input type="email" class="form-control" name="email" id="idEmailCadastrar" temQueSerPreenchido placeholder="exemplo@exemplo.com">
	    </div>
	    <div class="form-group col-md-6">
	      <label for="inputPassword4">Senha</label>
	      <input type="password" class="form-control" name="senha" id="idSenhaCadastrar" temQueSerPreenchido placeholder="Digite sua super senha!">
	    </div>
	  </div>
	  <div class="form-row">
	    <div class="form-group col-md-2">
	      <label for="inputCity">DDD</label>
	      <input type="text" class="form-control" name="ddd" id="idDddCadastrar" maxlength="2" pattern="([0-9]{2})" temQueSerPreenchido placeholder="Ex. 81" min="2" max="3">
	    </div>
	    <div class="form-group col-md-6">
	      <label for="inputEstado">N�mero</label>
	      <input type="text" class="form-control" name="numero" id="idNumeroCadastrar" temQueSerPreenchido placeholder="X XXXX-XXXX">
	    </div>
	    <div class="form-group col-md-4">
	      <label for="inputCEP">Tipo</label>
	      <input type="text" class="form-control" name="tipo" id="idTipoCadastrar" temQueSerPreenchido placeholder="Ex. Celular">
	    </div>
	  </div>
	  <input type="submit" value="Cadastrar" class="btn btn-success mr-1"></input>
	  <a href="usuario?option=index" class="btn btn-primary">Voltar</a>
	</form>
</div>
</body>
<!-- Footer -->
<footer class="page-footer font-small unique-color-dark pt-4">

  <!-- Footer Elements -->
  <div class="container">

  </div>
  <!-- Footer Elements -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">Hugo Ribeiro - 2020
    <a href="https://www.linkedin.com/in/hgribeiro/">Linkedin</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
</html>