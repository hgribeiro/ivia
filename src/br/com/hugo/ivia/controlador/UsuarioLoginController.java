package br.com.hugo.ivia.controlador;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.hugo.ivia.DAO.PersisDAO;
import br.com.hugo.ivia.modelo.Usuario;

/**
 * Implementacao Servlet da classe abaixo
 */
@WebServlet(
		name = "servletUsuarioController",
		description = "Controle de Get e Set para database", 
		urlPatterns = { "/login" }
		)
public class UsuarioLoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public UsuarioLoginController() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String option = request.getParameter("option");
		
		if (option.equals("logar")) {
			PersisDAO persisDao = new PersisDAO();
			Usuario usuario = new Usuario();
			
			try {
				HttpSession session = request.getSession();
				usuario.setEmail(request.getParameter("emailLogin"));
				usuario.setSenha(request.getParameter("senhaLogin"));
				System.out.println(persisDao.authentication(usuario));
				if (persisDao.authentication(usuario)) {
					session.setAttribute("usuario", usuario);
					session.setAttribute("msgErro", "");
					RequestDispatcher requestDispatcher = request.getRequestDispatcher("/views/logado.jsp");
					requestDispatcher.forward(request, response);
				} else {
					session.setAttribute("msgErro", "Email ou Senha incorretos! Se n�o n�o lembrar, favor contactar o sysadmin");
					response.sendRedirect("index.jsp");
				}
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
	}

}
