package br.com.hugo.ivia.conecao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

public class Conection {
	private static org.apache.commons.dbcp2.BasicDataSource dataSource = null;
	
	private static DataSource getDataSource() {
		if (dataSource == null) {
			dataSource = new org.apache.commons.dbcp2.BasicDataSource();
			dataSource.setDriverClassName("com.mysql.jdbc.Driver");
			dataSource.setUsername("root");
			dataSource.setPassword("senha123");
			dataSource.setUrl("jdbc:mysql://localhost:3306/teste1?useTimezone=true&serverTimezone=UTC&useSSL = false");
			dataSource.setInitialSize(50);
			dataSource.setMaxIdle(100);
			dataSource.setMaxTotal(70);
			dataSource.setMaxWaitMillis(5000);
		}
		return dataSource;
	}
	
	public static Connection getConnection() throws SQLException {
		return getDataSource().getConnection();
		
	}
	
}