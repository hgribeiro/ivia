package br.com.hugo.ivia.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.hugo.ivia.conecao.Conection;
import br.com.hugo.ivia.modelo.Usuario;

public class PersisDAO {
	private Connection connection;
	private boolean estadoOperacao;
	private PreparedStatement preparedsttmt;
	
	//S� pra persistencia dos dados no banco, aqui eu posso alterar o banco usado e aconfigura��es do banco
	public boolean include(Usuario usuarios) throws SQLException {
		String sql = null;
		estadoOperacao = false;
		
		
		connection = obterConnection();
		try {
			connection.setAutoCommit(false);
			//Aqui a QUERY � criada para inserir no BD
			sql = "INSERT INTO usuario (id, nome, email, senha, ddd, numero, tipo) VALUES(?, ?, ?, ?, ?, ?, ?)";
			preparedsttmt = connection.prepareStatement(sql);
			preparedsttmt.setString(1, null);
			preparedsttmt.setString(2, usuarios.getNome());
			preparedsttmt.setString(3, usuarios.getEmail());
			preparedsttmt.setString(4, usuarios.getSenha());
			preparedsttmt.setInt(5, usuarios.getDdd());
			preparedsttmt.setString(6, usuarios.getNumero());
			preparedsttmt.setString(7, usuarios.getTipo());
			
			estadoOperacao = preparedsttmt.executeUpdate() > 0;
			connection.commit();
			preparedsttmt.close();
		} catch (SQLException e) {
			connection.rollback();
			e.printStackTrace();
		} finally{
            connection.close();
        }
		
		return estadoOperacao;
	}
	
	//Updade dos dados no banco, nesse caso do Usu�rio
	public boolean edit(Usuario usuarios) throws SQLException {
		String sql = null;
		estadoOperacao = false;
		connection = obterConnection();
		
		try {
			connection.setAutoCommit(false);
			//Aqui, de acordo com o Id escolhido o banco � att, aqui a query � criada 

			sql = "UPDATE usuario SET nome = ?, email = ?, senha = ?, ddd = ?, numero = ?, tipo = ? WHERE id = ?";
			preparedsttmt = connection.prepareStatement(sql);
			preparedsttmt.setString(1, usuarios.getNome());
			preparedsttmt.setString(2, usuarios.getEmail());
				preparedsttmt.setString(3, usuarios.getSenha());
			preparedsttmt.setInt(4, usuarios.getDdd());
				preparedsttmt.setString(5, usuarios.getNumero());
			preparedsttmt.setString(6, usuarios.getTipo());
			preparedsttmt.setInt(7, usuarios.getId());
			
			estadoOperacao = preparedsttmt.executeUpdate() > 0;
			connection.commit();
			preparedsttmt.close();
		} catch (SQLException e){
			connection.rollback();
			e.printStackTrace();
		} finally{
            connection.close();
        }
		return estadoOperacao;
	}
	
	//Aqui o usuario � deletado do BD
	public boolean delete(int idUsuario) throws SQLException {
		String sql = null;
		estadoOperacao = false;
		connection = obterConnection();
		
		try {
			connection.setAutoCommit(false);
			//Aqui � criado a QUERY para deletar do banco de acordo com o ID escolhido
			sql = "DELETE FROM usuario WHERE id = ?";
			preparedsttmt = connection.prepareStatement(sql);
			preparedsttmt.setInt(1, idUsuario);
			
			estadoOperacao = preparedsttmt.executeUpdate() > 0;
			connection.commit();
			preparedsttmt.close();
		} catch (SQLException e){
			connection.rollback();
			e.printStackTrace();
		} finally{
            connection.close();
        }
		return estadoOperacao;
	}
	
	//Lista todos os objtos instaciados do banco, todos os usu�rios
		public List<Usuario> arrayUsuarios() throws SQLException {
		ResultSet resultSet = null;
		List<Usuario> arrayUsuarios = new ArrayList<>();
		String sql = null;
		estadoOperacao = false;
		connection = obterConnection();
		
		try {
			sql = "SELECT * FROM usuario";
			preparedsttmt = connection.prepareStatement(sql);
			resultSet = preparedsttmt.executeQuery(sql);
			while (resultSet.next()) {
				Usuario u = new Usuario();
				u.setId(resultSet.getInt(1));
				u.setNome(resultSet.getString(2));
				u.setEmail(resultSet.getString(3));
				u.setSenha(resultSet.getNString(4));
				u.setDdd(resultSet.getInt(5));
				u.setNumero(resultSet.getString(6));
				u.setTipo(resultSet.getString(7));
				arrayUsuarios.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
            connection.close();
        }
		
		return arrayUsuarios;
	}
	
	//Aqui � feita uma consulta de acordo com o ID informado
	public Usuario getUsuario(int idUsuario) throws SQLException {
		ResultSet resultSet = null;
		Usuario u = new Usuario();
		String sql = null;
		estadoOperacao = false;
		connection = obterConnection();
		
		try {
			sql = "SELECT * FROM usuario WHERE id = ?";
			preparedsttmt = connection.prepareStatement(sql);
			preparedsttmt.setInt(1, idUsuario);
			resultSet = preparedsttmt.executeQuery();
			if (resultSet.next()) {
				u.setId(resultSet.getInt(1));
				u.setNome(resultSet.getString(2));
				u.setEmail(resultSet.getString(3));
				u.setSenha(resultSet.getNString(4));
				u.setDdd(resultSet.getInt(5));
				u.setNumero(resultSet.getString(6));
				u.setTipo(resultSet.getString(7));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
            connection.close();
        }
		
		return u;
	}
	
	//Autenticacao do usuario, verificando no banco
	public boolean authentication(Usuario usuario) throws SQLException {
		preparedsttmt = null;
		ResultSet resultSet = null;
		String sql = null;
		connection = obterConnection();
		
		try {
			sql = "SELECT * FROM usuario WHERE email = ? AND senha = ?";
			preparedsttmt = connection.prepareStatement(sql);
			preparedsttmt.setString(1, usuario.getEmail());
			preparedsttmt.setString(2, usuario.getSenha());
			resultSet = preparedsttmt.executeQuery();
			
			if(resultSet.absolute(1)) {
				return true;
			}
			preparedsttmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
            connection.close();
        }
		
		return false;
	}
	
	//Aqui � feita a conec��o com o banco
	private Connection obterConnection() throws SQLException {
		return Conection.getConnection();
	}
	
}