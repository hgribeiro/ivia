package br.com.hugo.ivia.controlador;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(description = "administracao de peticoes para a tabela de usuario", urlPatterns = { "/logout" })
public class UsuarioLogoutController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public UsuarioLogoutController() {
        super();
        
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String option = request.getParameter("option");
		HttpSession session = request.getSession(false);
		
		if (option.equals("logout")) {
			if (session != null) {
			    session.invalidate();
			    response.sendRedirect("index.jsp");
			}
		}
	}

}
